
# The power GitLab Auto DevOps and Kubernetes

Recently I presented a [webinar about CI/CD and GitLab](https://amazicworld.com/webinar-the-benefits-of-ci-cd/). It contained a demo of the Auto DevOps feature of GitLab. Connected to a Kubernetes cluster, this Auto DevOps feature really impressed me. Setting this up appeared to be rather easy. Hence I would like to share how you can do this, so you can play around with this Auto DevOps feature of GitLab yourself.

***Why should anyone experience the power of GitLab's Auto DevOps feature with Kubernetes himself?***

Because, when you push just a simple Java Spring Boot helloworld app to your GitLab repository. The Auto DevOps feature gives you out-of-the-box an impressive, full fledged, very powerful standard pipeline. This pipeline isn't only complete in terms of testing, scanning, security and performance. But it also deploys your code on your Kubernetes cluster and can expose it at a public domain with TLS enabled.

## Prerequisites

Before we start, there are a couple of prerequisites. You need two accounts and three CLI's:

- [AWS account](https://console.aws.amazon.com/console/home)
- [AWS CLI](https://aws.amazon.com/cli/), [configured to your account](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-quickstart.html)
- [Kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/)
- [Eksctl](https://eksctl.io/): simple CLI to create an EKS cluster
- GitLab account with [30 days gold trail](https://about.gitlab.com/free-trial/)
- [Optional: Registered domain in AWS](https://console.aws.amazon.com/route53/home#DomainListing:).
  Adding a domain name to this setup really makes the Auto DevOps feature shine.

Remark: There are minor costs involved in AWS for this setup, e.g. EKS, Load Balancer, route53.

## Plan

What we are going to do:

- Create EKS cluster
- Connect cluster to GitLab
- Install apps on cluster
- Optional: Connect public domain
- Enable Auto DevOps and Push code
- View Auto DevOps in action

## Create EKS cluster

The tool `eksctl` provides a very nice and easy CLI to create an EKS cluster. We use it to create an EKS cluster with name `eks-gitlab` in AWS region `eu-central-1`.

```shell
# Create EKS
eksctl create cluster --name=eks-gitlab --region=eu-central-1
# Verify
kubectl get po -A
```

Note that `eksctl` has updated your kubeconf with the new Kubernetes cluster, hence you can directly connect with kubectl. You can also verify your Kubernetes cluster in the [AWS console](https://console.aws.amazon.com/eks/home). 

## Connect Kubernetes cluster to GitLab

Let's first create a new blank project in GitLab.

- Go to: *https://gitlab.com/dashboard/projects > New Project > Create blank project > Project name: `gitlab-demo` > Create project*

Now we can connect our Kubernetes cluster to this new project by granting GitLab `cluster-admin` access.

- Go to: *Operations > Kubernetes > integrate with a cluster certificate > Connect existing cluster*

We need to fill in some cluster values here, which we can retrieve as follows:

```shell
# Kubernetes cluster name: eks-gitlab

# API URL
kubectl cluster-info | grep 'Kubernetes' | awk '/http/ {print $NF}'

# CA Certificate
# First: Get secret name
SECRET_NAME_CA=$(kubectl get secret -o 'jsonpath={.items[*].metadata.name}')
# Then: Get CA cert
kubectl get secret $SECRET_NAME_CA -o jsonpath="{['data']['ca\.crt']}" | base64 --decode

# Service token
# First: Create service account
cat << EOF | kubectl apply -f -
apiVersion: v1
kind: ServiceAccount
metadata:
  name: gitlab
  namespace: kube-system
---
apiVersion: rbac.authorization.k8s.io/v1beta1
kind: ClusterRoleBinding
metadata:
  name: gitlab-admin
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
  - kind: ServiceAccount
    name: gitlab
    namespace: kube-system
EOF
# Second: Get secret name
SECRET_NAME_CA=$(kubectl -n kube-system get secret | grep gitlab | awk '{print $1}') 
# Then: Get service token
kubectl -n kube-system get secret $SECRET_NAME_CA -o jsonpath="{['data']['token']}" | base64 --decode

```

Then push "Add Kubernetes cluster" to create the connection.

## Install apps on Kubernetes

Now that GitLab has control over your cluster, it can install apps for you. These apps are installed with a single click.

- Go to: *Operations > Kubernetes > eks-gitlab > Applications*

For this demo it is enough to install:

- Ingress - route traffic to your web app
- Cert Manager - automatically create TLS certificate for your web app

But feel free to discover other apps as well, like:

- Prometheus - tool to monitor deployed applications
- GitLab Runner - ensures the pipeline agents will run inside your Kubernetes cluster instead of running in the GitLab cloud environment.

## Optional: Connect public domain

GitLab can use a public domain to expose your web app.

- If you do not have a registered domain in AWS, [create one](https://console.aws.amazon.com/route53/home#DomainListing:).

Note 1: AWS says it can take 3 days before the domain is ready. Mine took just 15 minutes.

Note 2: AWS will also create a hosted zone for your domain.

When you installed Ingress in the previous step. Kubernetes has created a [load balancer in AWS](https://console.aws.amazon.com/ec2/v2/home?#LoadBalancers). We will connect the domain to the load balancer by creating a CNAME DNS record.

- Copy the DNS name of your AWS load balancer.

- Go to: *[Route 53](https://console.aws.amazon.com/route53/v2/home#Dashboard) > Hosted zones > \[your domain\] > Create record*

  - Record policy: Simple routing
  - Record name: *.\[your domain\] (I put here my domain: `*.c3s.io`)
  - Record type: CNAME - Routes traffic to another domain name and to some AWS resources
  - Value: \[Paste the DNS name of your AWS load balancer\]

- Then push `Create records` to create the CNAME DNS record.

When the domain is connected to the load balancer, we need to configure the domain name in GitLab.

- Go to: *Operations > Kubernetes > eks-gitlab > Base domain: \[your domain\] (e.g. `c3s.io`)*

Note: Do not add the wildcard `*` to the domain here.

## Enable Auto DevOps and Push code

Everything is ready to enable Auto DevOps.

- Go to: *Settings > CI/CD > Auto DevOps > Expand*

- Enable the checkbox "Default to Auto DevOps pipeline" and save your changes.

Now you can push your code to your new repo. [Here is an simple Java Spring Boot helloworld example](https://gitlab.com/Dirc/spring-boot-helloworld) which you can use, but you can also choose a helloworld web app of any other popular language.

Note that this repo does not contain any Dockerfile, Helm chart or even a ci/cd config file. It is just a simple Java Spring Boot app with two tests defined and a standard pom file.

## Auto DevOps pipeline highlights

The moment you push your code a new pipeline is started:

View the pipeline at: *CI/CD > Pipelines*

This standard pipeline has an amazing set of features, like:

- build based on buildpacks (builds code, builds Docker image and creates Helm chart)
- Static code analysis
- Container scanning
- Dependency Scanning
- License Scanning
- Secret Detection
- Static application security testing (SAST)

It then creates a `dast` namespace in you Kubernetes cluster, to deploy your app and perform:

- Dynamic Application Security Testing (DAST)

And then, when all of this is succeeded, it deploys your app into the `production` namespace in your cluster. An makes it available at

`https://dirc-gitlab-demo.c3s.io`

Or more general, at: `https://<gitlab-group>-<gitlab-repo>.<your-domain>`

You can also view your environments at: *Operations > Environments*

Now change some code and make a merge request to see that this triggers a similar pipeline, but now it is deployed to a branch related namespace. So you can compare it easily to your web app that is running in namespace `production`. When you are happy with your change, approve the merge request and your change will be deployed in production.

Note: All of the above pipeline [steps consist of open source software](https://docs.gitlab.com/ee/topics/autodevops/stages.html) and can be [configured](https://docs.gitlab.com/ee/topics/autodevops/customize.html) if needed.

## Conclusion

Recall that the helloworld repo did not contain any ci/cd config files. GitLab Auto DevOps together with Kubernetes gives you a very mature and powerful standard pipeline.
Off course, every project will need it's own tweaks and additions, but this standard pipeline gives DevOps teams just a very good and easy starting point for setting up their CI/CD.

## References

- [Connect GitLab to existing Kubernetes cluster](https://docs.gitlab.com/ee/user/project/clusters/add_remove_clusters.html#add-existing-cluster)
- [Details of all pipeline steps](https://docs.gitlab.com/ee/topics/autodevops/stages.html)
- [Customize pipelines steps](https://docs.gitlab.com/ee/topics/autodevops/customize.html)






## ToDo

- [x] public domain in AWS
- [x] Kubernetes or k8s? or both?
- [x] Fix all ToDo
- [x] critical review the use of "really"
- [x] remark on the wildcard?
- [x] Spelling checker
- [x] All "Go to: path > ..." same layout

- [ ] @Marc: review op de inleiding, conclusie en algemene layout.
- [ ] mail blog to all webinar attandees? (contact Amazic?)
- [ ] All ABN Amro colleagues from Mike?
- [ ] Add blog link to repo readme: https://gitlab.com/Dirc/spring-boot-helloworld
