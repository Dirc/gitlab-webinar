
# PPT

## Introducion

Hi, I'm Eric and I'm a DevOps Engineer or what I prefere more is a CI/CD engineer.
Intro Cinq

## CI/CD

CI/CD is all about the process to get from code to running in production.

Picture: Code -> production

In essence, there or three goals:

- time to market
- code quality (also security)
- easy process (usage, maintenance)

These goals can be reached using:

- time to market
  - automation
  - shift-left (early bug detection)
- quality
  - tests
  - scanning
  - security
- process maintenance
  - standardize and simplify

Hoe uit zich dat dan?

## Typical use case

Typical customer of Cinq

Company has

- central CI/CD team
- support multiple tools
  - life cycle management
  - integration
- create standard pipelines

---> typical team where I end up
---> make more concrete

PPT Picture: multiple tools

Toolchain tax of multiple tools:

-> concrete problemen benoemen die we bij klanten zien

- friction and overhead
- fragile integrations
- hard to scale
- different licenses/costs
- lack of transparency

## Central teams

In general what we see at our customers is that they have a central CI/CD team which

- Maintain multiple tools (integrations, lifecycle)
- Create standard pipelines
- CI/CD consultancy to DevOps teams

There are a couple of pitfalls/challenges here and I want to emphasize two.

### Challenge 1: Tool choice

First of all, you need to choose the right tools.
You can do Infra with code using Bash, but it's better to choose a tool which is designed for the task, e.g. Terraform.
But there is a balance. New tools require take time to implement properly, require new/more knowledge, maintenance and increase integrations.

There is no silver bullet. Invest in knowledge since the CI/CD landscape is progressing and changing rapidly. Take time to experiment with new tooling, try to keep up to date and aknowledge this is neccesary.

To me, that is one of the strengths of a DevOps consultancy firm like CINQ. Customers expect consultants to introduce new ideas and developments. In the DevOps unit, we all have our own expertise and interests. We regularly chat on Slack about new tools, features or nice articles. We have knowledge sharing sessions where we do workshops and show each other new developments and we attend tech conferences. That is how we keep up to date with the DevOps industry and that is how can introduce new ideas and developments and add value to our customers.

### Challenge 2: Standard pipelines

Secondly, according to the devops philosophy, the devops teams should be responsible for the CI/CD themselfs. But off course, you do not want every team to reinvent the wheel. So there is a balance which basicly comes down to:

- how mature are the devops teams?
- how mature is the central ci/cd team?

Example:
On my current assignment at ABN AMRO, I'm in a devops team which is responsible for HashiCorp Vault, i.e. a third party application. We are running Vault on Kubernetes for which we need a couple of containers. So we need a CI pipeline for our containers to

- build
- tests
- security scanning
- push to registry

And we need a CD pipeline to deploy on k8s using Helm

- syntax checks
- push to registry
- deploy
- smoke test

The central CI/CD team of ABN AMRO has created standard pipelines

- Docker
- Helm
- Java
- Python
- (and more)

These pipelines are

- inner-sourced (everyone can contribute)
- easy to use (single page description)
- support multi-stage pipelines

And they are very good. They include things like:

- build
- tests
- static code checks
- security scanning
- versioning standard (very nice automatic SemVer)
- push to registry

PRO:

- quality is so good, it becomes a default choice for most devops teams
- taylored to company CI/CD toolset

CON:

- need very good engineers
- takes quite some time to develop
- maintenance (as always + in-house requires more maintenance)

Most companies do not have such a luxeroes CI/CD team and you do not have the resources to invest that heavily in knowledge and manpower?

GitLab:

In that case, in my opinion, another good choice could be GitLab. GitLab offers standard pipelines with a rich set of features. Here are some pipeline features you get out off the box:

- code quality checks
- container scanning
- dependency scanning
- license manager
- SAST Static Application Security Testing
- Review environment (running and expose your webapp on k8s)
- DAST Dynamic App Security Testing
- Performance Testing

Off course there is still some work to do:

- central CI/CD team
  - integrations with company environment
  - maintenance, lifecycle
- devops teams have to make these features work for your applications, which require config files.

But these standard pipelines really offers a very good starting point for devops teams to implement their CI/CD.

## GitLab

All-in-one tool

You always need to be carefull and a bit skeptical when a tool is clameing to be all-in-one.
But in this case, I believe GitLab is doing an impressive job.

## Standard pipeline feature

Standard pipeline (extremely high standard)
highlights:

- build code directly in container
- code quality checks
- container scanning
- dependency scanning
- license manager
- SAST Static Application Security Testing
- Review: running app (on k8s)
- DAST Dynamic App Security Testing
- Performance Testing

## Recall how to reach the CI/CD goals

- time to market
  - automation
  - shift-left (early bug detection)
- quality
  - tests
  - scanning
  - security
- process maintenance
  - standardize and simplify

---> it really checks all the boxes

## enterprise pipeline demo

...

### Vault integration

### Q&A
