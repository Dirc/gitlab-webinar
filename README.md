
# GitLab Webinar

Date: donderdag 26 november
Audience: Product owners, managers, engineers

## Summary

What are the goals of CI/CD, how can they be reached? A great variety of tools are used and often managed by a central CI/CD team who integrate, standardize and maintain them.
As a CI/CD consultant, I have created numerous pipelines, but none of them was so complete and mature as the Auto DevOps pipelines templates from GitLab.

## Agenda

- Intro
- CI/CD general goals and how to reach
- Typical use case example and it's problems
- How GitLab solves these problems
- How GitLab reaches the CI/CD goals: Auto DevOps feature
- Demo: Auto DevOps
- Q&A

## Reading / References

- All pipeline steps: https://docs.gitlab.com/ee/topics/autodevops/stages.html
- Independent blog on auto devops: https://medium.com/@wijnandtop/gitlabs-auto-devops-java-spring-boot-with-quality-control-to-production-in-minutes-using-7afdbc859b9a
- Spring boot project: https://github.com/JavaExamples/spring-boot-helloworld
- only 7 months old demo: https://www.youtube.com/watch?v=7q9Y1Cv-ib0
- Old Gitlab demo:
  - https://about.gitlab.com/blog/2018/08/10/gitlab-auto-devops-in-action/
  - https://www.youtube.com/watch?v=nMAgP4WIcno&feature=youtu.be&mkt_tok=eyJpIjoiTkRObE9URXhNMlEzTmpWbSIsInQiOiJaYkVDMjY0Q2RwSXQ0alV3V2xsRVBTVnlBVXdFXC9VWFd1Q3hIK25lbVpCWGVtdDZZMEVLWGRoRSt0UVBaOXVHMDBPeEp6RWUrb3VYdmtzYzZWejJOc1hQRW5tVUJqTm9HTTNUNGV4dG5zM3Z2d2RaclVFdmFCR1NaMjA3dnJyUUsifQ%3D%3D
- auto devops explained + 45min video: https://about.gitlab.com/blog/2019/10/07/auto-devops-explained/

- Customize pipelines: https://docs.gitlab.com/ee/topics/autodevops/customize.html

- [GitLab Partner page](https://partners.gitlab.com/English/Partner/home.aspx)
- [GitLab Partner Assets](https://partners.gitlab.com/prm/English/s/assets?id=221709)

- https://eksctl.io/
- https://docs.gitlab.com/ee/user/project/clusters/add_remove_clusters.html#add-existing-cluster
